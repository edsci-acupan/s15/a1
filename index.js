console.log('Hi')
// Details
const details = {
    firstName: "Riyanne Franczesca",
    lastName: "Acupan",
    age: 17,
    hobbies : [
        "arts", "music", "playing games"
    ] ,
    workAddress: {
        housenumber: "Lot 5, Block 1",
        street: "Hill Street",
        city: "Fictional City",
        state: "Nonexistent State",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.firstName)
console.log("My Last Name is " + details.lastName)
console.log(`Yes, I am ${details.firstName} ${details.lastName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");